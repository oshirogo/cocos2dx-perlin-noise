#ifndef __PARTICLE_SCENE_H__
#define __PARTICLE_SCENE_H__

#include "cocos2d.h"
#include "ui/CocosGUI.h"

USING_NS_CC;

class Particle : public cocos2d::ui::Widget
{
public:
    virtual bool init();
    void initForceField(Vec2 **_forceField, int _cols, int _rows);
    virtual void update(float);
    
    void follow();
    
    
    // implement the "static create()" method manually
    CREATE_FUNC(Particle);
private:
    DrawNode *particleNode;
    Vec2 prevPos;
    Vec2 pos;
    Vec2 vel;
    Vec2 acc;
    void checkEdges();
    Vec2 **forceField;
    int cols;
    int rows;
    
};

#endif // __PARTICLE_SCENE_H__
