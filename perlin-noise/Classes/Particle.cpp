#include "Particle.h"
#include <math.h>

USING_NS_CC;

// on "init" you need to initialize your instance
bool Particle::init()
{
    auto visibleSize = Director::getInstance()->getVisibleSize();
    
    pos = Vec2(rand() % (int)visibleSize.width, rand() % (int)visibleSize.height);
    prevPos = pos;
    vel = Vec2(rand() % 2, rand() % 2);
    acc = Vec2(0, 0);
    
    particleNode = DrawNode::create();
    this->addChild(particleNode);
    
    this->scheduleUpdate();
    
    return true;
}

void Particle::initForceField(Vec2 **_forceField, int _cols, int _rows)
{
    forceField = _forceField;
    cols = _cols;
    rows = _rows;
}

void Particle::update(float delta)
{
    
    follow();
    
    pos += vel;
    vel += acc;
    acc *= 0;
    
    int maxSpeed = 2;
    
    if(vel.x > maxSpeed) vel.x = maxSpeed;
    if(vel.y > maxSpeed) vel.y = maxSpeed;
    if(vel.x < -maxSpeed) vel.x = -maxSpeed;
    if(vel.y < -maxSpeed) vel.y = -maxSpeed;
    
    //particleNode->clear();
    //particleNode->drawDot(pos, 5, Color4F(0, 0, 0, 1));
    particleNode->drawLine(prevPos, pos, Color4F(0, 0, 0, 0.25));
    
    checkEdges();
    
    prevPos = pos;
}

void Particle::checkEdges()
{
    auto visibleSize = Director::getInstance()->getVisibleSize();
    if(pos.x > visibleSize.width) pos.x = 0;
    if(pos.x < 0) pos.x = visibleSize.width;
    if(pos.y > visibleSize.height) pos.y = 0;
    if(pos.y < 0) pos.y = visibleSize.height;
}

void Particle::follow()
{
    auto visibleSize = Director::getInstance()->getVisibleSize();
    float xPos = pos.x / visibleSize.width;
    float yPos = pos.y / visibleSize.height;
    int x = (int)clampf(floor(cols * xPos), 0, cols - 1);
    int y = (int)clampf(floor(rows * yPos), 0, rows - 1);
    Vec2 force = forceField[x][y];
    acc += force;
}

