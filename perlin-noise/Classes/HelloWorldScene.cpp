#include "HelloWorldScene.h"
#include <math.h>

USING_NS_CC;

Scene* HelloWorld::createScene()
{
    // 'scene' is an autorelease object
    auto scene = Scene::create();
    
    // 'layer' is an autorelease object
    auto layer = HelloWorld::create();

    // add layer as a child to scene
    scene->addChild(layer);

    // return the scene
    return scene;
}

// on "init" you need to initialize your instance
bool HelloWorld::init()
{
    
    srand(std::time(0));
    
    int seed = rand() % 1000;
    PerlinNoise pn(seed);
    
    //////////////////////////////
    // 1. super init first
    if ( !LayerColor::initWithColor(Color4B(255, 255, 255, 255)) )
    {
        return false;
    }
    
    auto visibleSize = Director::getInstance()->getVisibleSize();
    
    
    gridSize = 5;
    gridWidth = visibleSize.width / gridSize;
    gridHeight = visibleSize.height / gridSize;
    int numCols = floor(gridWidth / gridSize);
    int numRows = floor(gridHeight / gridSize);
    
    forceField = new Vec2*[numCols];
    for(int x=0; x < numCols; x++)
    {
        double sampleX = (float)x / (float)gridWidth;
        forceField[x] = new Vec2[numRows];
        for(int y=0; y<numRows; y++)
        {
            double sampleY = (float)y / (float)gridHeight;
            double perlinValue = pn.noise(sampleX, sampleY, 0.8) * 2 - 1;
            forceField[x][y] = Vec2(cos(perlinValue * 2 * M_PI), sin(perlinValue * 2 * M_PI)) * 0.15;
        }
    }
    
    particles = new Particle*[numParticles];
    
    for(int i = 0; i < numParticles; i++)
    {
        particles[i] = Particle::create();
        particles[i]->initForceField(forceField, numCols, numRows);
        this->addChild(particles[i]);
    }
    
    return true;
}
