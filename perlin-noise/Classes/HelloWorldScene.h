#ifndef __HELLOWORLD_SCENE_H__
#define __HELLOWORLD_SCENE_H__

#include "cocos2d.h"
#include "Particle.h"
#include "PerlinNoise.h"

class HelloWorld : public cocos2d::LayerColor
{
public:
    static cocos2d::Scene* createScene();

    virtual bool init();
    
    // implement the "static create()" method manually
    CREATE_FUNC(HelloWorld);
private:
    const int numParticles = 100;
    Particle **particles;
    Vec2 **forceField;
    int gridSize, gridWidth, gridHeight;
    PerlinNoise pn;
    
};

#endif // __HELLOWORLD_SCENE_H__
