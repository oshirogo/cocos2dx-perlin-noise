cocos2dx-perlin-noise
============

The Coding Train put together a great video on creating a perlin noise field in p5.js.
Here is a walkthrough for how to create the same effect in Cocos2d-x.

The source code from my video is available here: https://bitbucket.org/oshirogo/cocos2dx-perlin-noise

The Perlin Noise C++ implementation I used is here: https://github.com/sol-prog/Perlin_Noise

The original Coding Train tutorial video is here: https://www.youtube.com/watch?v=BjoM9oKOAKY

Information on Cocos2d-x is here: http://www.cocos2d-x.org/

Feel free to use the source code as you wish, under the terms of GPL:
http://www.gnu.org/copyleft/gpl.html

